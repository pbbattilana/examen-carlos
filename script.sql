CREATE DATABASE `construccion`;

CREATE TABLE `albañil` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) NOT NULL,
  `apellido` varchar(45) NOT NULL,
  `nro_docuento` varchar(45) NOT NULL,
  `obra` int(5) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `obra` (`obra`),
  CONSTRAINT `albañil_ibfk_1` FOREIGN KEY (`obra`) REFERENCES `obra` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

CREATE TABLE `obra` (
  `id` int(5) NOT NULL,
  `descripcion` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
