package py.com.construccion.servicio;

import java.util.List;
import py.com.construccion.domain.Obra;

public interface ObraService {
    
    public List<Obra> listarObras();
    public void guardar(Obra obra);
    public void eliminar(Obra obra);
    public Obra encontrarObra(Obra obra);
}
