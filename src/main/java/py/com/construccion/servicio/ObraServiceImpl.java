package py.com.construccion.servicio;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import py.com.construccion.dao.ObraDAO;
import py.com.construccion.domain.Obra;

@Service
public class ObraServiceImpl implements ObraService{
    
    @Autowired
    private ObraDAO obraDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Obra> listarObras() {
        return (List<Obra>) obraDAO.findAll();
    }

    @Override
    @Transactional()
    public void guardar(Obra obra) {
        obraDAO.save(obra);
    }

    @Override
    @Transactional()
    public void eliminar(Obra obra) {
        obraDAO.delete(obra);
    }

    @Transactional(readOnly = true)
    @Override
    public Obra encontrarObra(Obra obra) {
        return obraDAO.findById(obra.getId()).orElse(null);
    }
    
}
