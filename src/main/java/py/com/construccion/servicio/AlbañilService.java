package py.com.construccion.servicio;

import java.util.List;
import py.com.construccion.domain.Albañil;

public interface AlbañilService {
    
    public List<Albañil> listarAlbañiles();
    public void guardar(Albañil albañil);
    public void eliminar(Albañil albañil);
    public Albañil encontrarAlbañil(Albañil albañil);
}
