package py.com.construccion.servicio;

import java.util.List;
import py.com.construccion.domain.Albañil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import py.com.construccion.dao.AlbañilDAO;

@Service
public class AlbañilServiceImpl implements AlbañilService{
    
    @Autowired
    private AlbañilDAO albañilDAO;

    @Override
    @Transactional(readOnly = true)
    public List<Albañil> listarAlbañiles() {
        return (List<Albañil>) albañilDAO.findAll();
    }

    @Override
    @Transactional()
    public void guardar(Albañil albañil) {
        albañilDAO.save(albañil);
    }

    @Override
    @Transactional()
    public void eliminar(Albañil albañil) {
        albañilDAO.delete(albañil);
    }

    @Transactional(readOnly = true)
    @Override
    public Albañil encontrarAlbañil(Albañil albañil) {
        return albañilDAO.findById(albañil.getId()).orElse(null);
    }
    
}
