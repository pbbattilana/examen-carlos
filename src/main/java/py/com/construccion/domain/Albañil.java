package py.com.construccion.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name="albañil")
public class Albañil implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    private Integer id;
    
    @Basic(optional=false)
    @NotNull
    @Column(name="nombre", nullable=false)
    private String nombre;
    
    @Basic(optional=false)
    @NotNull
    @Column(name="apellido")
    private String apellido;
    
    @Basic(optional=false)
    @NotNull
    @Column(name="nro_docuento")
    private String nroDocumento;
    
    @JoinColumn(name = "obra", referencedColumnName = "id", nullable = false)
    @ManyToOne(targetEntity=Obra.class,optional=false, fetch = FetchType.EAGER)
    private Obra obra;
    

}
