package py.com.construccion.domain;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
@Entity
@Table(name="obra")
public class Obra implements Serializable{
    private static final long serialVersionUID = 1L;
    
    @Id
    //@GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id", nullable=false)
    private Integer id;
    
    @Basic(optional=false)
    @NotNull
    @Column(name="descripcion", nullable=false)
    private String descripcion;
}
