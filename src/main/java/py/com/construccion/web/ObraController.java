package py.com.construccion.web;

import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import py.com.construccion.domain.Obra;
import py.com.construccion.servicio.ObraService;

@Controller
@Slf4j
@RequestMapping("/obra")
public class ObraController {

    @Autowired
    private ObraService obraService;
    
//    @GetMapping("/")
//    public String inicio(Model model){
//        var obras = obraService.listarObras();
//        log.info("Ejecutando el controlador Sping MVC");
//        model.addAttribute("obras", obras);
//        
//        return "index";
//    }
    
    @GetMapping("/agregar")
    public String agregar(Obra obra, Model model){
        model.addAttribute("obra", obra);
        return "modificar_obra";
    }
    
    @PostMapping("/guardar")
    public String guardar(@Valid Obra obra, Errors errores){
        if(errores.hasErrors()){
            return "modificar_obra";
        }
        obraService.guardar(obra);
        return "redirect:/";
    }
    
    @GetMapping("/editar/{id}")
    public String editar(Obra obra, Model model){
        obra = obraService.encontrarObra(obra);
        model.addAttribute("obra", obra);
        return "modificar_obra";
    }
    
    @GetMapping("/eliminar")
    public String eliminar(Obra obra){
        obraService.eliminar(obra);
        return "redirect:/";
    }
}
