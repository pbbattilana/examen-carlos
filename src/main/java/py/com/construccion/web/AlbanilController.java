package py.com.construccion.web;

import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import py.com.construccion.domain.Albañil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import py.com.construccion.servicio.AlbañilService;
import py.com.construccion.servicio.ObraService;

@Controller
@Slf4j
@RequestMapping("/albanil")
public class AlbanilController {

    @Autowired
    private AlbañilService albanilService;
    @Autowired
    private ObraService obrasService;
    
//    @GetMapping("/")
//    public String inicio(Model model){
//        var albaniles = albanilService.listarAlbañiles();
//        var obras = obrasService.listarObras();
//        log.info("Ejecutando el controlador Sping MVC");
//        model.addAttribute("albaniles", albaniles);
//        model.addAttribute("obras", obras);
//        
//        return "index";
//    }
    
    @GetMapping("/agregar")
    public String agregar(Albañil albanil, Model model){
        model.addAttribute("albanil", albanil);
        return "modificar";
    }
    
    @PostMapping("/guardar")
    public String guardar(@Valid Albañil albanil, Errors errores){
        if(errores.hasErrors()){
            return "modificar";
        }
        albanilService.guardar(albanil);
        return "redirect:/";
    }
    
    @GetMapping("/editar/{id}")
    public String editar(Albañil albanil, Model model){
        albanil = albanilService.encontrarAlbañil(albanil);
        model.addAttribute("albanil", albanil);
        return "modificar";
    }
    
    @GetMapping("/eliminar")
    public String eliminar(Albañil albanil){
        albanilService.eliminar(albanil);
        return "redirect:/";
    }
}
