package py.com.construccion.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import py.com.construccion.servicio.AlbañilService;
import py.com.construccion.servicio.ObraService;

@Controller
@Slf4j
public class IndexController {

    @Autowired
    private AlbañilService albanilService;
    @Autowired
    private ObraService obrasService;
    
    @GetMapping("/")
    public String inicio(Model model){
        var albaniles = albanilService.listarAlbañiles();
        var obras = obrasService.listarObras();
        log.info("Ejecutando el controlador Sping MVC");
        model.addAttribute("albaniles", albaniles);
        model.addAttribute("obras", obras);
        
        return "index";
    }
}
