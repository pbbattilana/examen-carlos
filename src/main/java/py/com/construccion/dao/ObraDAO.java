package py.com.construccion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import py.com.construccion.domain.Obra;

public interface ObraDAO extends JpaRepository<Obra, Integer>{
    
}
