package py.com.construccion.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import py.com.construccion.domain.Albañil;

public interface AlbañilDAO extends JpaRepository<Albañil, Integer>{
    
}
